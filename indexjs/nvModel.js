function NhanVien(
  taiKhoan,
  hoTen,
  email,
  matKhau,
  date,
  luongCB,
  chucVu,
  gioLam
) {
  this.user = taiKhoan;
  this.name = hoTen;
  this.email = email;
  this.password = matKhau;
  this.date = date;
  this.salary = luongCB;
  this.position = chucVu;
  this.workHour = gioLam;
  this.tinhLuong = function () {
    var Luong = 0;
    if (this.position == "Sếp") {
      Luong = this.salary * 1 * 3;
      return Luong;
    } else if (this.position == "Trưởng phòng") {
      Luong = this.salary * 1 * 2;
      return Luong;
    } else if (this.position == "Nhân viên") {
      Luong = this.salary * 1 * 1;
      return Luong;
    }
  };
  this.xepLoai = function () {
    var xepLoai = "";
    if (this.workHour >= 192) {
      xepLoai = "Xuất sắc";
      return xepLoai;
    }
    if (this.workHour >= 176) {
      xepLoai = "Giỏi";
      return xepLoai;
    }
    if (this.workHour >= 160) {
      xepLoai = "Khá";
      return xepLoai;
    } else {
      xepLoai = "Trung bình";
      return xepLoai;
    }
  };
}
