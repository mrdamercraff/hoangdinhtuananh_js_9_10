function kiemTraTrung(id,dsnv){
    var index = timKiemViTriArr(id,dsnv);
    if(index !== -1){
        showMessageErr('tbTKNV',"tài khoản không được trùng")
        return false;
    }else{
        showMessageErr('tbTKNV','');
        return true;
    }

}
function kiemTraUser(value){
    let reg = /^(?=[0-9]{4,6}$)0*[1-9][0-9]{3,}$/
    var isUser = reg.test(value);
    if(isUser){
        showMessageErr('tbTKNV','')
        return true
    }else{
        showMessageErr('tbTKNV','tài khoản chỉ gồm 4-6 ký số')
        return false;
    }

}
function kiemTraTrong(userInput,idErr){
    if(userInput.length==0){
        showMessageErr(idErr,'cột này không được để trống');
        return false
    }else{
        showMessageErr(idErr,'')
        return true;
    }

}
function removeAscent (str) {
    if (str === null || str === undefined) return str;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    return str;
  }
function kiemTraTen(value,idErr){
   
        var re = /^[a-zA-Z!@#\$%\^\&*\ )\(+=._-]{2,}$/g; 
        var isName = re.test(removeAscent(value))
      
    if(isName){
        showMessageErr(idErr,'');
        return true;
    }else{
        showMessageErr(idErr,'tên chỉ chứa kí tự chữ')
        return false;
    }
}
function kiemTraEmail(value){
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    var isEmail = re.test(value);
    if(isEmail){
        showMessageErr('tbEmail','')
        return true;
    }else{
        showMessageErr('tbEmail',"email không đúng định dạng")
    }
}
function kiemTraMatKhau(value){
    var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
    var isPassword = re.test(value);
    if(isPassword){
        showMessageErr('tbMatKhau','')
        return true;
    }else{
        showMessageErr('tbMatKhau','mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)')
        return false;
    }
}
function kiemTraNgay(value){
    var re = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
    var isDate = re.test(value);
    if(isDate){
        showMessageErr('tbNgay','');
        return true;
    }else{
        showMessageErr('tbNgay','ngày tháng không đúng định dạng')
    }
}
function kiemTraLuong(value){
    var re = /^\d+$/;
    var isSalary = re.test(value);
    if(isSalary && value*1>=1000000 && 20000000>=value*1){
        showMessageErr('tbLuongCB','');
        return true;
    }else{
        showMessageErr('tbLuongCB','lương cơ bản 1000000-20000000')
        return false;
    }
}
function kiemTraChucVu(value){
    
    if(value=="Chọn chức vụ"){
        showMessageErr('tbChucVu','chọn chức vụ hợp lệ');
        return false;
    }else{
        showMessageErr('tbChucVu','')
        return true;
    }
}
function kiemTraGioLam(value){
    var re = /^\d+$/;
    var isHour = re.test(value);
    if(isHour && value*1>=80&& 200>=value*1){
        showMessageErr('tbGiolam','')
        return true;
    }else{
        showMessageErr('tbGiolam','Số giờ làm trong tháng 80-200 giờ')
    }
}
function kiemTraSearch(value){
    if(value ==="Xuất sắc" || value === "Khá" || value === "Trung bình" || value ==="Giỏi"){
        showMessageErr("tbSearch",'')
        return true;
    }else{
        showMessageErr('tbSearch',`Từ khoá tìm kiếm bao gồm:Xuất sắc, Giỏi, Khá, Trung bình`)
        return false;
    }
}