var dsnv = [];
var searchList = [];
var DSNV = "DSNV";
var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
  var nvArr = JSON.parse(dataJson);
  for (index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new NhanVien(
      item.user,
      item.name,
      item.email,
      item.password,
      item.date,
      item.salary,
      item.position,
      item.workHour
    );
    dsnv.push(nv);
  }
  renderArr(dsnv);
}
function luuLocalStorage() {
  let jasonDsnv = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, jasonDsnv);
}
function themNv() {
  var nv = themThongTinVaoForm();
  var isValid = true;
  isValid =
    kiemTraTrong(nv.user, "tbTKNV") &&
    kiemTraUser(nv.user) &&
    kiemTraTrung(nv.user, dsnv);
  isValid =
    isValid & (kiemTraTrong(nv.name, "tbTen") && kiemTraTen(nv.name, "tbTen"));
  isValid =
    isValid & (kiemTraTrong(nv.email, "tbEmail") && kiemTraEmail(nv.email));
  isValid =
    isValid &
    (kiemTraTrong(nv.password, "tbMatKhau") && kiemTraMatKhau(nv.password));
  isValid = isValid & (kiemTraTrong(nv.date, "tbNgay") && kiemTraNgay(nv.date));
  isValid =
    isValid & (kiemTraTrong(nv.salary, "tbLuongCB") && kiemTraLuong(nv.salary));
  isValid = isValid & kiemTraChucVu(nv.position);
  isValid =
    isValid &
    (kiemTraTrong(nv.workHour, "tbGiolam") && kiemTraGioLam(nv.workHour));
  if (isValid) {
    dsnv.push(nv);
    renderArr(dsnv);
    let jasonDsnv = JSON.stringify(dsnv);
    localStorage.setItem(DSNV, jasonDsnv);
  }
}
function xoaNv(user) {
  var viTri = timKiemViTriArr(user, dsnv);
  if (viTri !== -1) {
    dsnv.splice(viTri, 1);
    renderArr(dsnv);
  }
  luuLocalStorage();
}

function suaNv(user) {
  var viTri = timKiemViTriArr(user, dsnv);
  if (viTri == -1) {
    return;
  }
  var data = dsnv[viTri];
  showThongTinLenForm(data);
  document.getElementById("tknv").disabled = true;
}

function capNhatNv() {
  var nv = themThongTinVaoForm();
  var viTri = timKiemViTriArr(nv.user, dsnv);
  if (viTri == -1) return;
  var isValid = true;
  isValid =
    kiemTraTrong(nv.user, "tbTKNV") &&
    kiemTraUser(nv.user) &&
    kiemTraTrung(nv.user, dsnv);
  isValid =
    isValid & (kiemTraTrong(nv.name, "tbTen") && kiemTraTen(nv.name, "tbTen"));
  isValid =
    isValid & (kiemTraTrong(nv.email, "tbEmail") && kiemTraEmail(nv.email));
  isValid =
    isValid &
    (kiemTraTrong(nv.password, "tbMatKhau") && kiemTraMatKhau(nv.password));
  isValid = isValid & (kiemTraTrong(nv.date, "tbNgay") && kiemTraNgay(nv.date));
  isValid =
    isValid & (kiemTraTrong(nv.salary, "tbLuongCB") && kiemTraLuong(nv.salary));
  isValid = isValid & kiemTraChucVu(nv.position);
  isValid =
    isValid &
    (kiemTraTrong(nv.workHour, "tbGiolam") && kiemTraGioLam(nv.workHour));
  if (isValid) {
    dsnv[viTri] = data;
    renderArr(dsnv);
  }
  luuLocalStorage();
  resetForm();
}

function timNv() {
  var searchNv = document.getElementById("searchName").value;
  //   const newArray = [];
  //   for (var index = 0; index < dsnv.length; index++) {
  //     var item = dsnv[index];
  //     if (item.xepLoai().includes(searchNv)) {
  //       var nvSearch = new NhanVien(
  //         item.user,
  //         item.name,
  //         item.email,
  //         item.password,
  //         item.date,
  //         item.salary,
  //         item.position,
  //         item.workHour
  //       );
  //       newArray.push(nvSearch);
  //     }
  //   }
  //   dsnv.forEach(item => {
  //     if (item.xepLoai().includes(searchNv)) {
  //         const nvSearch = new NhanVien(
  //           item.user,
  //           item.name,
  //           item.email,
  //           item.password,
  //           item.date,
  //           item.salary,
  //           item.position,
  //           item.workHour
  //         );
  //         newArray.push(nvSearch);
  //       }
  //   });
  let isValid = true;
  isValid = isValid + kiemTraSearch(searchNv);
  if(isValid){
  const newArray = dsnv.filter((item) => item.xepLoai().includes(searchNv));
  
  renderArr(newArray);}
}
