function themThongTinVaoForm() {
  var taiKhoan = document.querySelector("#tknv").value;
  var hoTen = document.querySelector("#name").value;
  var email = document.querySelector("#email").value;
  var matKhau = document.querySelector("#password").value;
  var date = document.querySelector("#datepicker").value;

  var luongCB = document.querySelector("#luongCB").value;
  var chucVu = document.querySelector("#chucvu").value;
  var gioLam = document.querySelector("#gioLam").value;
  var Nv = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    date,
    luongCB,
    chucVu,
    gioLam
  );
  return Nv;
}
function renderArr(dsnv) {
  var contentHTML = "";
  for (var index = 0; index < dsnv.length; index++) {
    var curentNv = dsnv[index];
    var contentLi = `
         
     <tr>
         <td>${curentNv.user}</td>
         <td>${curentNv.name}</td>
         <td>${curentNv.email}</td>
         <td>${curentNv.date}</td>
         <td>${curentNv.position}</td>
         <td>${curentNv.tinhLuong()}</td>
         <td>${curentNv.xepLoai()}</td>
 
         <td><button onclick ='xoaNv("${
           curentNv.user
         }")' class='btn btn-danger'>Xoá</button>
        <button onclick ='suaNv("${curentNv.user}")' class='btn btn-warning' data-toggle="modal"
        data-target="#myModal">Sửa</button></td>
     </tr>
     `;
    contentHTML += contentLi;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function timKiemViTriArr(user, arrNv) {
  for (var index = 0; index < arrNv.length; index++) {
    var item = arrNv[index];
    if (item.user == user) {
      return index;
    }
  }
  return -1;
}

function showThongTinLenForm(nv) {
  document.querySelector("#tknv").value = nv.user;
  document.querySelector("#name").value = nv.name;
  document.querySelector("#email").value= nv.email;

  document.querySelector("#datepicker").value= nv.date;

  document.querySelector("#luongCB").value=nv.salary;
  document.querySelector("#chucvu").value=nv.position
  document.querySelector("#gioLam").value=nv.workHour;
}

function resetForm(){
    document.querySelector('#formNv').reset();
    document.getElementById('tknv').disabled = false;
}
function showMessageErr (idErr, message){
    document.getElementById(idErr).innerHTML =message;
    document.getElementById(idErr).style.display = "block";
}
